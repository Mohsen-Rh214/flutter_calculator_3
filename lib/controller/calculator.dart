import 'package:get/get.dart';
import 'package:ultimate_calculator/model/button_model.dart';

class Calculator extends GetxController {
  var firstNumber = 0;
  var secondNumber = 0;
  String operator = '';
  String result = '0';

  void onPressed(Button button, ButtonType type) {
    get(button, type);
  }
  void backSpace(){
    // firstNumber.toString().substring(1);
    // print(firstNumber);
    // update();
  }
  void get(Button button, ButtonType type) {
    switch (type) {
      case ButtonType.number:
        if (operator == '') {
          if (firstNumber == 0) {
            firstNumber = int.parse(button.value);
            update();
          } else if (firstNumber != 0) {
            firstNumber = int.parse(firstNumber.toString() + button.value);
            update();
          }
        } else if (operator != '') {
          if (secondNumber == 0) {
            secondNumber = int.parse(button.value);
            update();
          } else if (secondNumber != 0) {
            secondNumber = int.parse(secondNumber.toString() + button.value);
            update();
          }
        }
        break;
      case ButtonType.operator:
        if (firstNumber == 0) {
          update();
        } else if (firstNumber != 0 && secondNumber == 0) {
          operator = button.value;
          update();
        } else if (secondNumber != 0) {
          calculate();
          firstNumber = int.parse(result);
          operator = button.value;
          secondNumber = 0;
          update();
        }
        break;
      case ButtonType.result:
        calculate();
        break;
      case ButtonType.clear:
        result = 0.toString();
        firstNumber = 0;
        secondNumber = 0;
        operator = '';
        update();
        break;
    }
  }

  void calculate(){
    switch (operator) {
      case '+':
        result = (firstNumber + secondNumber).toString();
        update();
        break;
      case '-':
        result = (firstNumber - secondNumber).toString();
        update();
        break;
      case '×':
        result = (firstNumber * secondNumber).toString();
        update();
        break;
      case '÷':
        result = (firstNumber / secondNumber).toStringAsFixed(1);
        update();
        break;
      case '^':
        result = (firstNumber * firstNumber).toString();
        update();
        break;
      case '%':
        result = ((firstNumber / secondNumber) * 100).toString();
        update();
        break;
    }
  }
}
