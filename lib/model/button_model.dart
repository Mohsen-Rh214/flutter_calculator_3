enum ButtonType { number, operator, result, clear }

class Button {
  String value;

  Button({required this.value});
}
