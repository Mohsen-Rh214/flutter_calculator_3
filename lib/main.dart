import 'package:flutter/material.dart';
import 'package:ultimate_calculator/view/button_pad.dart';
import 'package:ultimate_calculator/view/output_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ultimate Calculator',
      theme: ThemeData(
      ),
      home: UltimateCalculator(),
    );
  }
}
 class UltimateCalculator extends StatelessWidget {
   const UltimateCalculator({Key? key}) : super(key: key);

   @override
   Widget build(BuildContext context) {
     return Scaffold(
       body: Column(
         children: [OutputScreen(),ButtonPad()],
       ),
     );
   }
 }
