import 'package:flutter/material.dart';
import 'package:ultimate_calculator/model/button_model.dart';

import 'button.dart';

class ButtonPad extends StatelessWidget {
  const ButtonPad({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SmartButton(
                  button: Button(value: 'AC'),
                  type: ButtonType.clear,
                ),
                SmartButton(
                  button: Button(value: '^'),
                  type: ButtonType.operator,
                ),
                SmartButton(
                  button: Button(value: '%'),
                  type: ButtonType.operator,
                ),
                SmartButton(
                  button: Button(value: '×'),
                  type: ButtonType.operator,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SmartButton(
                  button: Button(value: '7'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '8'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '9'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '÷'),
                  type: ButtonType.operator,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SmartButton(
                  button: Button(value: '4'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '5'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '6'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '+'),
                  type: ButtonType.operator,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SmartButton(
                  button: Button(value: '1'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '2'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '3'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '-'),
                  type: ButtonType.operator,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BackSpaceButton(),
                SmartButton(
                  button: Button(value: '0'),
                  type: ButtonType.number,
                ),
                SmartButton(
                  button: Button(value: '.'),
                  type: ButtonType.operator,
                ),
                SmartButton(
                  button: Button(value: '='),
                  type: ButtonType.result,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
