import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ultimate_calculator/controller/calculator.dart';

class OutputScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size.height;
    var kTextStyle = TextStyle(fontSize: 80);

    return GetBuilder<Calculator>(
        init: Calculator(),
        builder: (calculator) {
          return Container(
            color: Colors.white,
            child: Container(
              height: size * 0.4,
              width: size,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        '${calculator.firstNumber == 0 ? '' : calculator.firstNumber}',
                        style: kTextStyle,
                      ),
                      Text(
                        calculator.operator == '' ? '' : calculator.operator,
                        style: kTextStyle,
                      ),
                      Text(
                        calculator.secondNumber == 0
                            ? ''
                            : calculator.secondNumber.toString(),
                        style: kTextStyle,
                      ),
                    ],
                  ),
                  Text(
                    calculator.result,
                    style: TextStyle(
                      fontSize: 100,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
