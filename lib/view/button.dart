import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ultimate_calculator/controller/calculator.dart';
import 'package:ultimate_calculator/model/button_model.dart';

final calculator = Get.put(Calculator());

class SmartButton extends StatelessWidget{
  SmartButton({required this.button, required this.type});

  Button button;
  ButtonType type;


  @override
  Widget build(BuildContext context) {
    var size = 90.0;
    return Padding(
      padding: EdgeInsets.all(3),
      child: MaterialButton(
        onPressed: (){
          calculator.onPressed(button,type);
        },
        elevation: 5,
        minWidth: size,
        height: size,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        color: type == ButtonType.number
            ? Colors.blue
            : type == ButtonType.operator
                ? Colors.pink
                : type == ButtonType.result ? Colors.blueGrey : Colors.deepOrangeAccent ,
        child: Text(button.value, style: TextStyle(fontSize: 30),),
        textColor: Colors.white,
      ),
    );
  }
}

class BackSpaceButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var size = 90.0;
    return Padding(
      padding: EdgeInsets.all(3),
      child: MaterialButton(
        onPressed: () {
          calculator.backSpace();
        },
        elevation: 0,
        minWidth: size,
        height: size,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        color: Colors.transparent,
        child: Icon(Icons.backspace_outlined, size: 47,),
        textColor: Colors.blueGrey,
      ),
    );
  }
}
